var main = {
    operacion:null,
    canvas:null,
    contexto:null,
    w:null,
    h:null,
    // Tamaño de los cuadros.
    tamCuadro:null,
    // Dirección de la serpiente.
    direccion:null,
    // Alimento de la serpiente.
    comida:null,
    // Puntaje del juego.
    puntaje:null,
    // Variable que representa la serpiente.
    serpiente:null,  
    jsonResponse:null,
    velocidad:null,
    estadoLogin:null,
    tail:null,
    /**
     * Función de inicio de cariables.
     * @returns {undefined}
     */
    init:function(){
        // Canvas en el que se dibuja todo el juego.
        main.canvas = $("#canvasGame")[0];
        main.contexto = main.canvas.getContext("2d");
        main.w = $("#canvasGame").width();
        main.h = $("#canvasGame").height();
        main.tamCuadro = 10;
        main.velocidad=150;
    },
    
    /**
     * Función de inicio de ejecución del juego.
     * @returns {undefined}
     */
    login:function(){
        var dific;
        if(main.velocidad==150){
            dific ="Medio";
        }else if(main.velocidad ==200){
            dific ="Rapido";
        }else{
            dific ="Rapido";
        }
        $("#responseDificultad").text(dific);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "login",
                nombreusuario: $("#nombreusuario").val(),
                dificultad: dific
            },
            success: function (dataR) {
                main.jsonResponse = JSON.parse(dataR);
                $("#responseNombreUsuario").text("Usuario: " + main.jsonResponse.nombreusuario);
                $("#responseMaxPuntaje").text("MaxPuntaje: " + main.jsonResponse.puntajemaxusuario);
                $("#responseDificultad").text("Dificultad: " + main.jsonResponse.dificultadusuario);
            },
            error: function (e) {
            }
        });
    },
    
    /**
    * Función que ejecuta el juego por primera vez.
    */
    jugar:function() {
        if($("#nombreusuario").val() == ""){
            alert("Debe ingresar su nombre de jugador primero.");
            main.estadoLogin = false;
        }else{
            main.estadoLogin = true;
            // Dirección inicial por defecto.
            main.direccion = "right";
            main.crear_serpiente();
            main.crear_comida();
            main.puntaje = 0;
            if (typeof game_loop != "undefined") clearInterval(game_loop);
               game_loop = setInterval( main.dibujar,  main.velocidad);
        }
    },
        
    /**
    * Función que crea la serpiente.
    */
    crear_serpiente:function() {
        main.serpiente = [];
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "crearSerpiente",
            },
            success: function (dataR) {
                var jsonResponse = JSON.parse(dataR);
                var taman = jsonResponse.taman;
                main.serpiente.push({
                    x: jsonResponse.x1,
                    y: jsonResponse.y1
                });
                main.serpiente.push({
                    x: jsonResponse.x2,
                    y: jsonResponse.y2
                });
                main.serpiente.push({
                    x: jsonResponse.x3,
                    y: jsonResponse.y3
                });
            }
        });
    },
    
    /**
    * Función que crea la serpiente.
    */
    crear_comida:function() {
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "crearComida",
            },
            success: function (dataC) {
                var jsonResponse = JSON.parse(dataC);
                main.comida = {
                    x: jsonResponse.x,
                    y: jsonResponse.y
                };
            }
        });
    },
    
    /**
    * Función que dibuja en el canvas el mapa del juego.
    */
    dibujar:function() {
        main.contexto.clearRect(0, 0, main.canvas.width, main.canvas.height);
        main.contexto.strokeStyle = "hsl(0,0%,10.3%)";
        for (var i = 0; i < (main.canvas.height / 10); i++) {
            for (var j = 0; j < (main.canvas.width / 10); j++) {
                 main.contexto.strokeRect(10 * i, 10 * j, 10, 10);
            };
        }
        var posX =  main.serpiente[0].x;
        var posY =  main.serpiente[0].y;
     //   main.colision(posX, posY,  main.serpiente);
        if ( main.direccion == "right") posX++;
        else if ( main.direccion == "left") posX--;
        else if ( main.direccion == "up") posY--;
        else if ( main.direccion == "down") posY++;
        main.colision(posX, posY,  main.serpiente);
        if (posX == - 1 || posX ==  main.w /  main.tamCuadro || posY == - 1 || posY ==  main.h /  main.tamCuadro) {
         //   $('#songs2')[0].play();
            $('#Pausa').hide();
            $('#Reanudar').hide();
            $('#Jugar').hide();
            $('#Reiniciar').show();
            clearInterval(game_loop);
            return;
        }
        main.comer(posX, posY, main.comida.x, main.comida.y);
    },

    /**
    * Función que dibuja cada celda del mapa de juego.
    * @param {int} x es la posición en el eje X de la celda.
    * @param {int} y es la posición en el eje Y de la celda.
    */
    dibujar_celda:function(x, y) {
        var num = Math.floor(Math.random() * (230 + 1));
        var color = "rgba(" + num + ", 255, 0, 1)";
        color = "#fff";
        main.contexto.fillStyle = color;
        main.contexto.fillRect(x *  main.tamCuadro, y *  main.tamCuadro,  main.tamCuadro,  main.tamCuadro);
        main.contexto.strokeStyle = "hsl(0,0%,10.3%)";
        main.contexto.strokeRect(x *  main.tamCuadro, y *  main.tamCuadro,  main.tamCuadro,  main.tamCuadro);
    },
    
    /**
    * Función que verifica si la serpiente ha comido algo.
    * @param {int} posX es la posición en el eje X de la cabeza de la serpiente.
    * @param {int} posY es la posición en el eje Y de la cabeza de la serpiente.
    * @param {int} comidaX es la posición en el eje X de la comida.
    * @param {int} comidaY es la posición en el eje Y de la comida.
    * @return {boolean} true si está la cabeza esta en la misma posición de la comida.
    */
    comer:function(posX, posY, comidaX, comidaY) {
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "comer",
                posX: posX,
                posY: posY,
                comidaX:comidaX,
                comidaY:comidaY,
                puntaje:main.puntaje
            },
            success: function (dataC) {
                var jsonResponse = JSON.parse(dataC);
                if(jsonResponse.comer==="true"){
                    main.tail = {
                        x: posX,
                        y: posY
                    };
                    //   $('#songs')[0].play();
                    main.puntaje=jsonResponse.puntaje;
                    main.crear_comida();
                } else {
                    main.tail =  main.serpiente.pop();
                    main.tail.x = posX;
                    main.tail.y = posY;
                }
                
                main.serpiente.unshift(main.tail);
                for (var i = 0; i < main.serpiente.length; i++) {
                    var c = main.serpiente[i];
                    main.dibujar_celda(c.x, c.y);
                }
                main.dibujar_celda(main.comida.x, main.comida.y);
                var puntaje_text = "Puntaje: " + main.puntaje;
                $("#puntaje").html(puntaje_text);
            }
        });
    },
    
    /**
    * Función que verifica si la serpiente se colisiona.
    * @param {int} x es la posición en el eje X de la celda.
    * @param {int} y es la posición en el eje Y de la celda.
    * @param {int} serpiente es el arreglo que representa la serpiente.
    * @return {boolean} true si está en colisión y false si es lo contrario.
    **/
    colision:function(x,y,serpiente){
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "colision",
                x: x,
                y: y,
                serpiente:JSON.stringify(serpiente)
            },
            success: function (dataC) {
                var jsonResponse = JSON.parse(dataC);
                if(jsonResponse.colision==="true"){
                    $('#Pausa').hide();
                    $('#Reanudar').hide();
                    $('#Jugar').hide();
                    $('#Reiniciar').show();
                    clearInterval(game_loop);
                    return;
                }
            }
        });
    }
}

/**
* Función que permite cambiar de dirección a la serpiente.
*/
$(document).keydown(function(e) {
    var tecla = e.which;
    if ((tecla == "37" || tecla == "65") &&  main.direccion != "right")  main.direccion = "left";
    else if ((tecla == "38" || tecla == "87") &&  main.direccion != "down")  main.direccion = "up";
    else if ((tecla == "39" || tecla == "68") &&  main.direccion != "left")  main.direccion = "right";
    else if ((tecla == "40" || tecla == "83") &&  main.direccion != "up")  main.direccion = "down";
})

/**
* Función principal del juego, la
* cual permite acceder a las funciones Jugar,
* Pausa y Reanudar.
*/
$(document).ready(function() {
    main.init();
      $('#Pausa').hide();
      $('#Reanudar').hide();
      $('#Reiniciar').hide();
    $("#Jugar").click(function() {
        main.jugar();
        if(main.estadoLogin == true){
            $('#Jugar').hide();
            $('#Pausa').show();
            $('#Reanudar').hide();
        }
    });
    $("#btnLogin").click(function() {
        if($("#nombreusuario").val()==""){
            alert("Se requiere un nombre.");
        }else{
            main.login();
            $("#btnExitLogin").click();
            $("#Nivel").hide();
            $("#Login").hide();
        }
    });
    $('#Pausa').click(function() {
        clearInterval(game_loop);
        $('#Pausa').hide();
        $('#Reanudar').show();
    });
    $('#Reanudar').click(function() {
        clearInterval(game_loop);
        $('#Pausa').show();
        $('#Reanudar').hide();
        game_loop = setInterval(main.dibujar, 160);
    });
    $('#Reiniciar').click(function() {
        main.jugar();
        $('#Jugar').hide();
        $('#Pausa').show();
        $('#Reanudar').hide();
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'http://localhost:8080/WebServiceRestSnakeCliente/SSnake',
            data: {
                orden: "actualizarMaxPuntaje",
            },
            success: function (dataC) {
                var jsonResponse = JSON.parse(dataC);
                $("#responseMaxPuntaje").text("MaxPuntaje: " + jsonResponse.puntajemaxusuario);
            }
        });
    });

    $('#facil').click(function() {
        main.velocidad = 200;
    });
    
    $('#medio').click(function() {
        main.velocidad = 150;
    });

    $('#dificil').click(function() {
        main.velocidad = 10;
    });
});