
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Carlos Aguirre.
 */
public class SnakeGame {
    
    private int dificultad;
    private ArrayList<Coordenada> serpiente;
    
    public ArrayList<Coordenada> crear_serpiente(){
        // Tamaño inicial por defecto.
        int taman = 3;
        serpiente  = new ArrayList<Coordenada>();
        for (int i = (taman - 1); i >= 0; i = (i - 1)) {
             serpiente.add(new Coordenada(i,0));
        }
        return serpiente;
    }
    
    public Coordenada crear_comida() {
        int x = (int) Math.round(Math.random() * 44);
        int y = (int) Math.round(Math.random() * 44);
        Coordenada coor = new Coordenada(x,y);
        return coor;
    }
    
    public boolean colision(int x, int y,ArrayList<Coordenada> serpiente) {
        for (int i = 0; i < serpiente.size(); i++) {
            if (serpiente.get(i).getX() == x && serpiente.get(i).getY() == y){
                return true;
            }
        }
        return false;
    }
    
    public boolean comer(int posX, int posY, int comidaX, int comidaY){
        if (posX == comidaX && posY == comidaY) {
            return true;
        }
        return false;
    }
    
}
