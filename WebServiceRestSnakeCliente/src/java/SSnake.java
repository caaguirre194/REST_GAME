/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.cliente.MetodosPartida;
import com.cliente.MetodosUsuario;
import com.persistencia.Partida;
import com.persistencia.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;

/**
 *
 * @author Carlos Aguirre.
 */
public class SSnake extends HttpServlet {
    
    private Usuario usuarioON = new Usuario();
    private Partida partidaON = new Partida();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            SnakeGame game = new SnakeGame();
            ObjectMapper jsonMapper = new ObjectMapper();
            String orden = request.getParameter("orden");
              
            if(orden.equals("login")){
                response.setContentType("text/html;charset=UTF-8");
                
                MetodosUsuario ccc=new MetodosUsuario();
                MetodosPartida ppp=new MetodosPartida();
                String nombreUsuario = request.getParameter("nombreusuario");
                String dificultad = request.getParameter("dificultad");
 
                List<Usuario> data=ccc.buscarUsuarios();
                boolean agregar = true;
                for (Usuario user : data) {
                    if(user.getNombreusuario().equals(nombreUsuario)){
                        agregar = false;
                        usuarioON = user;
                        ppp.agregarPartida(user,dificultad);
                        break;
                    }
                }
                if(agregar == true){
                    ccc.agregarUsuario(nombreUsuario);
                    List<Usuario> data1=ccc.buscarUsuarios();
                    for (Usuario user : data1) {
                        if(user.getNombreusuario().equals(nombreUsuario)){
                            usuarioON = user;
                            ppp.agregarPartida(user,dificultad);
                            break;
                        }
                    }
                }
                
                List<Partida> dataP = ppp.buscarPartidas();
                ArrayList<Partida> p = new ArrayList<Partida>();
                for (Partida part : dataP) {
                    if(part.getIdusuario().getIdusuario() == usuarioON.getIdusuario()){
                        p.add(part);
                        partidaON.setIdpartida(part.getIdpartida());
                        partidaON.setIdusuario(usuarioON);
                        partidaON.setDificultad(dificultad);
                        partidaON.setPuntaje(0);
                    }
                }
                
                int maxPun=0;
                for(int i=0;i<p.size();i++){
                    int punt = p.get(i).getPuntaje();
                    if(maxPun<punt){
                        maxPun = punt;
                    }
                }

                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("nombreusuario",usuarioON.getNombreusuario());
                objectNode.put("puntajemaxusuario",""+maxPun);
                objectNode.put("dificultadusuario",""+partidaON.getDificultad());

                jsonMapper.writeValue(response.getWriter(), objectNode);
            
            }else if(orden.equals("crearSerpiente")){
                ArrayList<Coordenada> serp = game.crear_serpiente();
              
                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("taman", serp.size());
                objectNode.put("x1", serp.get(0).getX());
                objectNode.put("y1", serp.get(0).getY());
                objectNode.put("x2", serp.get(1).getX());
                objectNode.put("y2", serp.get(1).getY());
                objectNode.put("x3", serp.get(2).getX());
                objectNode.put("y3", serp.get(2).getY());

                jsonMapper.writeValue(response.getWriter(), objectNode);
                
            }else if(orden.equals("crearComida")){
                Coordenada comida = game.crear_comida();
                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("x", comida.getX());
                objectNode.put("y", comida.getY());
                jsonMapper.writeValue(response.getWriter(), objectNode);
            }else if(orden.equals("colision")){
                int x = Integer.parseInt(request.getParameter("x"));
                int y = Integer.parseInt(request.getParameter("y"));
                String serp_ = request.getParameter("serpiente");

                ObjectMapper mapper = new ObjectMapper();
                ArrayList<Coordenada> serpiente = mapper.readValue(serp_,new TypeReference<ArrayList<Coordenada>>(){});
                boolean colision=false;
                colision = game.colision(x, y, serpiente);

                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("colision", ""+colision);
                jsonMapper.writeValue(response.getWriter(), objectNode);
            }else if(orden.equals("comer")){
                int posX = Integer.parseInt(request.getParameter("posX"));
                int posY = Integer.parseInt(request.getParameter("posY"));
                int comidaX = Integer.parseInt(request.getParameter("comidaX"));
                int comidaY = Integer.parseInt(request.getParameter("comidaY"));
                int puntaje = Integer.parseInt(request.getParameter("puntaje"));

                boolean comer=false;
                comer = game.comer(posX, posY, comidaX, comidaY);
                
                if(comer == true){
                   MetodosPartida ppp=new MetodosPartida();
                   partidaON.setPuntaje(puntaje+1);
                   ppp.editPuntaje(partidaON);
                }
                
                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("comer", ""+comer);
                objectNode.put("puntaje", ""+(puntaje+1));
                jsonMapper.writeValue(response.getWriter(), objectNode);
            }else if(orden.equals("actualizarMaxPuntaje")){
                MetodosPartida ppp=new MetodosPartida();
                List<Partida> dataP = ppp.buscarPartidas();
                ArrayList<Partida> p = new ArrayList<Partida>();
                for (Partida part : dataP) {
                    if(part.getIdusuario().getIdusuario() == usuarioON.getIdusuario()){
                        p.add(part);
                    }
                }
                
                int maxPun=0;
                for(int i=0;i<p.size();i++){
                    int punt = p.get(i).getPuntaje();
                    if(maxPun<punt){
                        maxPun = punt;
                    }
                }
                
                ObjectNode objectNode = jsonMapper.createObjectNode();
                objectNode.put("puntajemaxusuario",""+maxPun);
                jsonMapper.writeValue(response.getWriter(), objectNode);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
